#!/bin/sh -e

apk add --no-cache git
rm -rf /bootstrap
git clone https://gitlab.com/jakesys/jn-bootstrap.git /bootstrap
cd /bootstrap
exec ./bootstrap.sh
