#!/bin/sh -e

# we expect Name to be the full external FQDN for the node
REQUIRED_TAGS="
    Name
    data_cache
    data_swap
"
# OPTIONAL_TAGS
#   node_labels         - additional node labels <k1>=<v1> <k2=<v2> ...
#   node_ssh            - switch to alternate SSH port
#   node_knock          - knock port to allow SSH
#   node_knock_iface    - apply knock on interface (default 'eth+')
#   services            - <name>:<port> ... for swarm dns/healthchecks

log() {
  echo -e "\n*** $(date +%FT%TZ) - $*" >&2
}

install_more_things() {
    apk add \
        aws-cli \
        bash \
        bind-tools \
        cachefilesd-inotify \
        coreutils \
        curl \
        docker \
        docker-cli-compose \
        efs-utils \
        gettext \
        grep \
        htop \
        mosh-server \
        procps \
        screen \
        sed \
        tcptraceroute \
        utmps
}

get_zone_id() {
    local z domain="$1."
    while [ -n "$domain" ]; do
        z="$(aws route53 list-hosted-zones-by-name \
                --output text --max-items 1 --dns-name "$domain" |
            grep HOSTEDZONES | cut -d/ -f3 | grep -F "$domain" | cut -f1
        )"
        [ -n "$z" ] && break
        domain="${domain#*.}"
    done
    if [ -z "$z" ]; then
        log "ERROR: Unable to find ZoneID for $1."
        return 1     # not found
    fi
    echo "$z"
}

shell_kv() {
    printf '%s="%s"\n' "$1" "$2"
}

save_instance_info() {
    local t tag val err
    REGION="$(imds meta-data/placement/region)"
    [ "$REGION" = "us-east-1" ] && DOMAIN_EC2="ec2.internal" || DOMAIN_EC2="compute.internal"
    FQDN="$(imds meta-data/tags/instance/Name)"
    NODE="$(echo "$FQDN" | cut -d. -f1)"
    SWARM="$(echo "$FQDN" | cut -d. -f2)"
    VCN="$(echo "$FQDN" | cut -d. -f3)"
    DOMAIN="$(echo "$FQDN" | cut -d. -f3-)"
    DOMAIN_INT="$(grep ^domain /etc/resolv.conf | cut -d' ' -f2)"
    log "Saving instance info"
    {
        shell_kv REGION "$REGION"
        shell_kv DOMAIN_EC2 "$DOMAIN_EC2"
        shell_kv AZ "$(imds meta-data/placement/availability-zone)"
        shell_kv INSTANCE_ID "$(imds meta-data/instance-id)"
        shell_kv INSTANCE_TYPE "$(imds meta-data/instance-type)"
        shell_kv IPV4 "$(imds meta-data/public-ipv4)"
        shell_kv IPV4_INT "$(imds meta-data/local-ipv4)"
        shell_kv IPV6 "$(imds meta-data/ipv6)"
        shell_kv FQDN "$FQDN"
        shell_kv FQDN_INT "$NODE.$SWARM.$DOMAIN_INT"
        shell_kv NODE "$NODE"
        shell_kv SWARM "$SWARM"
        shell_kv DOMAIN "$DOMAIN"
        shell_kv DOMAIN_INT "$DOMAIN_INT"
        shell_kv VCN "$VCN"
        shell_kv ZONE "$(get_zone_id "$DOMAIN")"
        shell_kv ZONE_INT "$(get_zone_id "$DOMAIN_INT")"
        for t in $(imds meta-data/tags/instance); do
            tag=TAG_$(echo "$t" | tr - _ | tr -cd 0-9a-zA-Z_)
            val="$(imds meta-data/tags/instance/"$t")"
            shell_kv "$tag" "$val"
        done
    } | sort > /etc/instance-info

    # make sure we got everything
    for t in $REQUIRED_TAGS; do
        if ! grep -q "^TAG_$t=" /etc/instance-info; then
            log "Missing required tag '$t'"
            err=1
        fi
    done
    [ -z "$err" ] || exit 1

    # auto-export these variables
    set -a
    . /etc/instance-info
    . /etc/tiny-cloud.conf
    set +a
}

setup_hostname() {
    log "Setting hostname, domainname etc."
    echo "$NODE" > /etc/hostname
    hostname -F /etc/hostname
    echo -e "127.0.1.2\t$FQDN_INT  $NODE" >> /etc/hosts

    # update resolv.conf domain to include the swarm
    sed -i -e "s/^domain .*/domain $SWARM.$DOMAIN_INT/" /etc/resolv.conf

    # (supposedly) tell dhcpcd to not overwrite resolv.conf
    #if ! grep -q "^nohook resolv.conf$" /etc/dhcpcd.conf 2>/dev/null; then
    #    printf "\n\nnohook resolv.conf\n" >> /etc/dhcpcd.conf
    #fi
    # NOTE: above does not work, so we did the following...
    #chattr +i /etc/resolv.conf
    echo "skip_hooks='20-resolv.conf'" > /usr/lib/dhcpcd/dhcpcd-hooks/00-skips

}

wait_dev() {
    local max="${2:-10}"
    while [ ! -e "/dev/$1" ] && [ "$max" -gt 0 ]; do
        sleep 1
        let "max--"
    done
    if [ ! -e "/dev/$1" ]; then
        log "Aborting - /dev/$vol doesn't exist"
        exit 1
    fi
}

setup_data_volume() {
    local swap="$1"
    local cache="$2"
    local vol="xvdb"
    if [ ! -e "/dev/$vol" ]; then
        vol="sdb"
    fi

    log "Partitioning swap/cache/data volume /dev/$vol"
    wait_dev "$vol"
    sfdisk /dev/"$vol" <<EOT
32,$((swap * 2097152 + 32)),S
,$((cache * 2097152)),L
,,L
EOT

    log "Setting up swap partition"
    wait_dev "$vol"1
    mkswap -L swap /dev/"$vol"1
    echo "LABEL=swap  swap  swap  defaults  0 0" >> /etc/fstab
    swapon -a

    log "Setting up cache partition"
    wait_dev "$vol"2
    mkfs.ext4 -L cache /dev/"$vol"2
    echo "LABEL=cache  /var/cache/fscache  ext4  defaults,noatime  1 1" >> /etc/fstab
    [ -e /var/cache/fscache ] && mv /var/cache/fscache /var/cache/fscache.old
    mkdir -p /var/cache/fscache
    mount /var/cache/fscache

    log "Setting up data partition"
    wait_dev "$vol"3
    mkfs.ext4 -L data /dev/"$vol"3
    echo "LABEL=data  /var/lib/docker  ext4  defaults,noatime  1 1" >> /etc/fstab
    [ -e /var/lib/docker ] && mv /var/lib/docker /var/lib/docker.old
    mkdir -p /var/lib/docker
    mount /var/lib/docker
}

setup_efs() {
    local accesspoint ap id setup

    log "Determining EFS Access Point"
    accesspoint="$(aws efs describe-access-points --output text |
        grep ^ACCESSPOINTS | grep -F "${VCN}_${SWARM}"
    )"
    if [ -z "$accesspoint" ]; then
        log "ERRROR: Unable to find ${VCN}_${SWARM} EFS accesspoint"
        return 1
    fi
    ap="$(echo "$accesspoint" | cut -f3)"
    id="$(echo "$accesspoint" | cut -f5)"

    log "Configuring nfsmount and starting cachefilesd"
    # efs is essentially nfs4...
    sed -i -e 's/,nfs4/,nfs4,efs/g' \
        -e 's/== "nfs4"/~ "^(nfs4|efs)$"/g' \
        /etc/init.d/nfsmount
    rc-update add nfsmount
    rc-update add cachefilesd
    service cachefilesd start

    log "Setting up EFS $id $ap to mount at $EFS_DIR"
    mkdir -p "$EFS_DIR"
    echo "$id  $EFS_DIR  efs  _netdev,tls,accesspoint=$ap,fsc  0 0" >> /etc/fstab
    log "Mounting EFS at $EFS_DIR"
    mount "$EFS_DIR"

    # is there a swarm node setup script to run?
    setup="$EFS_DIR/.swarm-node-setup"
    if [ -f "$setup" ] && [ -x "$setup" ]; then
        log "Running swarm node setup"
        "$setup"
    fi
}

swarm_save_node() {
    local iid_dir="$SWARM_DIR/nodes/$INSTANCE_ID"
    mkdir -p "$iid_dir"
    [ -n "$1" ] && echo "$1" > "$SWARM_DIR/token"
    echo "$NODE" >> "$iid_dir/node"
    NODE_ID="$(docker node inspect self --format "{{.ID}}")"
    echo "$NODE_ID" >> "$iid_dir/node_id"
    echo "$IPV4_INT" >> "$iid_dir/ipv4_int"
    echo "$IPV4" >> "$iid_dir/ipv4"
    echo "$IPV6" >> "$iid_dir/ipv6"
    echo "$FQDN_INT" >> "$iid_dir/fqdn_int"
    echo "$FQDN" >> "$iid_dir/fqdn"
    echo "$ZONE_INT" >> "$iid_dir/zone_int"
    echo "$ZONE" >> "$iid_dir/zone"
    # TODO: save other stuff too?
    cp /etc/instance-info "$iid_dir"    # does this have everything?
}

swarm_node_labels() {
    local kv
    log "Setting node labels"
    # optional
    for kv in ${TAG_node_labels:-}; do
        docker node update "$NODE_ID" \
            --label-add "$kv" >/dev/null
    done
    # mandatory
    docker node update "$NODE_ID" \
        --label-add az="$AZ" \
        --label-add region="$REGION" \
        --label-add instance_type="$INSTANCE_TYPE" >/dev/null
}

join_swarm() {
    local managers="$1"
    local ip token fail state

    token="$(cat "$SWARM_DIR/token")"
    # join one of the managers, using the token
    for ip in $managers; do
        fail=
        if ! ping -c1 -W1 "$ip" >/dev/null; then
            fail=1
            continue
        fi
        log "Attmempting to join swarm via $ip"
        docker swarm join --token "$token" "$ip":2377 >/dev/null && break
        while true; do
            # check join error
            state="$(docker info --format '{{.Swarm.LocalNodeState}}')"
            case "$state" in
                pending)
                    log "...pending background join"
                    sleep 1 ;;
                active)
                    log "...background join successful"
                    break ;;
                error)
                    log "...join failed: $(docker info --format '{{.Swarm.Error}}')"
                    fail=1
                    break ;;
                *)
                    log "...unknown .Swarm.LocalNodeState '$state'"
                    fail=1
                    break ;;
            esac
        done
        # if we're joined now, don't bother to try other managers
        [ -z "$fail" ] && break
    done
    if [ -n "$fail" ]; then
        log "ERROR: Unable to join any managers of $SWARM"
        return 1
    fi
    log "Successfully joined swarm $SWARM"
}

setup_docker() {
    local swarm_ips

    log "Configuring docker"

    # install some helper scripts and docker config
    install -t /usr/sbin -m 0500 root/sbin/*
    mkdir -p /etc/docker
    cp -a root/etc/docker/daemon.json /etc/docker

    # preload ip_vs module
    echo 'ip_vs' > /etc/modules-load.d/docker-swarm.conf
    modprobe ip_vs

    # make sure docker doesn't trash our IPv6 default route!
    cp -a root/etc/sysctl.d/docker-ipv6.conf /etc/sysctl.d

    # cloud user can do docker stuff
    addgroup "${CLOUD_USER:-alpine}" docker

    # NOTE: not needed for host net containers to answer IPv6
    #install -t /etc/docker -Dm644 root/etc/docker/daemon.json

    log "Looking for active Docker Swarm IPs"
    swarm_ips=$(swarm-ips setup) || return 1

    log "Starting docker"
    rc-update add docker default
    service cgroups start  # needed before first docker start
    service docker start

    log "Waiting for docker to be ready"
    while true; do
        docker ps >/dev/null 2>&1 && break
    done

    if [ -z "$swarm_ips" ]; then
        # initialize swarm
        log "Initializing Docker Swarm"
        docker swarm init > /dev/null
        swarm_save_node \
            "$(docker swarm join-token -q manager)"
    else
        join_swarm "$swarm_ips"
        swarm_save_node
    fi
    swarm_node_labels
    swarm-cleanup
    swarm-dns add
}

setup_knock() {
    local table="$1"
    local port="$2"
    local iface="${TAG_node_knock_iface:-eth+}"
    local i ipt

    for i in iptables ip6tables; do
        # docker IPv6 uses FORWARD instead of DOCKER-USER
        [ "$i" = "ip6tables" ] && [ "$table" = "DOCKER-USER" ] && table="FORWARD"
        ipt="$i -I $table -i $iface -p tcp --dport"
        log "Add SSH port $port knocking to $i $table"
        $ipt "$port" --syn -j DROP
        $ipt "$port" --syn -m recent --rcheck --seconds 60 --name knock -j ACCEPT
        # we only need one knock rule
        [ "$port" = 22 ] &&
            $ipt 985 -m recent --set --name knock
        service "$i" save
    done
}


### SETTINGS

install_more_things
save_instance_info
setup_hostname


### DATA VOLUME

setup_data_volume \
    "$TAG_data_swap" \
    "$TAG_data_cache"


### EFS VOLUME

EFS_DIR="/mnt/efs/$SWARM"
SWARM_DIR="$EFS_DIR/swarm"
setup_efs


### RE/CONFIGURE STUFF

## btmpd/utmpd/wtmpd
for s in btmpd utmpd wtmpd; do
    rc-update add "$s" boot
done

## syslog
log "Adjusting size of /var/log/syslog"
sed -i -e 's/^SYSLOGD_OPTS=.*/SYSLOGD_OPTS="-t -s 1024"/' \
    /etc/conf.d/syslog
service syslog restart

## iptables

[ -n "$TAG_node_knock" ] &&
    setup_knock INPUT "${TAG_node_ssh:-22}"

log "Setting up iptables/ip6tables"
for I in iptables ip6tables; do
  rc-update add "$I"
  service "$I" save
  service "$I" start
done

## docker (and DNS)
setup_docker

# normal SSH may be handled by a docker container
if [ -n "$TAG_node_ssh" ]; then
    # ...with knocking
    [ -n "$TAG_node_knock" ] &&
        setup_knock DOCKER-USER 22  # for shell containers

    log "Switch sshd to use port $TAG_node_ssh"
    sed -i -e 's/^#Port 22/Port '"$TAG_node_ssh"'/' \
        /etc/ssh/sshd_config
fi
log "Switch sshd to AllowTCPForwarding, and restart"
sed -i -e 's/^AllowTcpForwarding .*/AllowTcpForwarding yes/' \
    /etc/ssh/sshd_config
service sshd restart

# any of that work at all?
log "final iptables"
iptables -L -n >&2

log "Installing Handy Profile Stuff"
install -o root -g root -m 644 root/etc/profile.d/jn.sh /etc/profile.d

# get everything up-to-date
log "Upgrading Alpine Packages"
apk upgrade -a


### CLEANUP / FINALIZE / REBOOT

rm -rf /bootstrap

tiny-cloud --bootstrap complete
reboot
