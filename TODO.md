# TODO

* what to do about duplicate node names?
  * need to do anything special (or abort) if it's still managing and/or up?
  * demote old node (via node id!) if still manager
  * rm old node (via node id)

* split Route53 out to separate script(s)?

* how to handle healthchecks / swarm multivalue records?
  * maybe by service - have separate traefiks?
    * web
    * mail
    * ssh
